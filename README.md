#  notebook de calcul d'un indicator type de voie
## description : [indicator.yml FILE](https://gitlab.com/open-mobility-indicators/indicators/pedestrian-way-types/-/blob/main/indicator.yaml)

Calcul d'un indicateur "type de voie" à partir des données de voirie accessible aux piétons venant de fichiers OpenStreetMap au format pbf.
Le résultat est un fichier geoJSON contenant les tronçons classés en impasse, traverse, voirie interdite aux voitures, et autres voies (plus quelques données attributaires comme le nom de la voie ou la vitesse maxi autorisée quand elle est renseignée dans OSM). (l'infrastructure OMI produit ensuite des tuiles vecteur .mbtiles à partir du fichier geoJSON).

forked from [jupyter notebook template](https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook)

[Install locally using a venv or Docker, Build and Use (download, compute)](https://gitlab.com/open-mobility-indicators/website/-/wikis/2_contributeur_technique/install-a-notebook-locally)