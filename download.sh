#!/bin/bash
set -euo pipefail

function usage {
    echo "usage: download.sh <parameter_profile_file> <target_dir>"
    exit 1
}

if [ $# -ne 2 ]; then
    usage
fi
PARAMETER_PROFILE_FILE=$1
TARGET_DIR=$2

if [ ! -f $PARAMETER_PROFILE_FILE ]; then
    echo "Parameter profile file not found: $PARAMETER_PROFILE_FILE"
    usage
fi


if [ ! -d $TARGET_DIR ]; then
    echo "Target dir not found: $TARGET_DIR"
    usage
fi

echo "download data to target dir: $TARGET_DIR"

# Your code here

PBF_URL=`jq -r '.pbf_url' $PARAMETER_PROFILE_FILE`
PBF_FILE=`jq -r '.pbf_file' $PARAMETER_PROFILE_FILE`

echo "download data to target dir: $TARGET_DIR/$PBF_FILE"
wget -q "$PBF_URL" -O $TARGET_DIR/$PBF_FILE
echo "done."