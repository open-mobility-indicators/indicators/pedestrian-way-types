#!/bin/bash
# compute.sh
set -euo pipefail

function usage {
    echo "usage: compute.sh <parameter_profile_file> <source_dir> <target_dir>"
    exit 1
}

if [ $# -ne 3 ]; then
    usage
fi

PARAMETER_PROFILE_FILE=$1
SOURCE_DIR=$2
TARGET_DIR=$3

if [ ! -f $PARAMETER_PROFILE_FILE ]; then
    echo "Parameter profile file not found: $PARAMETER_PROFILE_FILE"
    usage
fi

if [ ! -d $SOURCE_DIR ]; then
    echo "Source dir not found: $SOURCE_DIR"
    usage
fi

if [ ! -d $TARGET_DIR ]; then
    echo "Target dir not found: $TARGET_DIR"
    usage
fi

echo "compute data from source dir: $SOURCE_DIR to target dir: $TARGET_DIR"

# Temp directory
TEMP_DIR=`mktemp -d`
echo "Temporary directory $TEMP_DIR created"

# Run notebook
papermill \
   -p PARAMETER_PROFILE_FILE $PARAMETER_PROFILE_FILE \
   -p SOURCE_DIR $SOURCE_DIR \
   -p TARGET_DIR $TARGET_DIR \
   --log-output \
   notebook.ipynb $TEMP_DIR/output.ipynb

if [ -d $TEMP_DIR ]; then
    echo "cleaning temp dir..."
    rm -fR $TEMP_DIR
fi

echo "done."
